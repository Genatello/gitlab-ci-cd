import random
import string


def generate_random_email():
    # Predefined list of domains and top-level domains
    domains = ["gmail", "yahoo", "outlook", "mail"]
    top_levels = ["com", "net", "org", "info"]

    # Generate a random name
    name_length = random.randint(5, 10)
    name = ''.join(random.choices(string.ascii_lowercase + string.digits, k=name_length))

    # Randomly choose a domain and a top-level domain
    domain = random.choice(domains)
    top_level = random.choice(top_levels)

    # Combine to form the email address
    email = f"{name}@{domain}.{top_level}"
    return email


# Generate and display a random email address
random_email = generate_random_email()
print(random_email)
